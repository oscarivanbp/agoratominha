using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestProviderName()
        {
            string result = Prueba.Services.Constants.ProviderName.MinhaCDN;
            Assert.AreEqual('"'+"MINHA CDN"+'"', result);
        }
        [TestMethod]
        public void TestProviderNameFalse()
        {
            string result = Prueba.Services.Constants.ProviderName.MinhaCDN;
            Assert.AreNotEqual("MINHA CDN", result);
        }
        [TestMethod]
        public void TestRed()
        {
            Prueba.Services.readRoute read = new Prueba.Services.readRoute();
            string url = "";
            string path = "";
            bool result = read.read( url, path);
            Assert.AreEqual(false, result);
        }
        [TestMethod]
        public void TestReadUrl()
        {
            Prueba.Services.readRoute read = new Prueba.Services.readRoute();
            string url = "https://s3.amazonaws.com/uux-itaas-static/minha-cdn-logs/input-01.txt";
            string path = "";
            bool result = read.read(url, path);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TestReadFile()
        {
            Prueba.Services.readRoute read = new Prueba.Services.readRoute();
            string url = "";
            string path = "input.txt";
            bool result = read.read(url, path);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void TestReadPath()
        {
            Prueba.Services.readRoute read = new Prueba.Services.readRoute();
            string url = "https://s3.amazonaws.com/uux-itaas-static/minha-cdn-logs/input-01.txt";
            string path = "input.txt";
            bool result = read.read(url, path);
            Assert.AreEqual(true, result);

        }


    }
     
}
