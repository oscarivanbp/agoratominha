﻿using System;
using Prueba.Services;
using Prueba.Formats;
using Prueba.models;


using System;
using System.IO;
using System.Net;
using System.Configuration;


namespace Prueba
{
    class Solution
    {
        protected AgoraFormatOrder FinalFormat { get; set; }

        public agoraFinalFormat ConvertMinha{ get; set; }

       public string StartConverter(string url, string path)
        {
            try
            {

            
                    if (Validators.InputsValid(url, path))
                    {
                        readRoute route = new readRoute();
                        if (route.read(url, path))
                        {
                            try
                            {
                                FinalFormat = new AgoraFormatService();
                                ConvertMinha = new MinhaCDN(FinalFormat);
                                AgoraToMinha convert = new AgoraToMinha(ConvertMinha);
                                string converted = convert.Execute(route.getFiles());
                                Console.WriteLine(converted);
                                string saveText = "";
                                saveText = route.save(converted);
                                if (saveText == "ok")
                                {
                                    Console.WriteLine("file saved successfully");
                                }
                                else
                                {
                                    Console.WriteLine("wrong saving file");
                                }

                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }

                        }

                }
                else
                {
                Console.WriteLine("Try again");
                    //Program.Main(null);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return "Completed";



        }



    }
}
