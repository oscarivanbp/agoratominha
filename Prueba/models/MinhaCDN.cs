﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prueba.Formats;


using System.Collections;


using static Prueba.Services.Constants;

namespace Prueba.models
{

    //protected AgoraFormatOrder FinalFormat { get; set; }
    class MinhaCDN : agoraFinalFormat
    {
        public Hashtable FieldSimple { get; set; }
        public Hashtable FieldComplex { get; set; }
        
        public string Provider { get { return ProviderName.MinhaCDN; } }
        public char Separator { get { return '|'; } }

        

        protected AgoraFormatOrder templateFormat { get; set; }
        public MinhaCDN(AgoraFormatOrder to)
        {
            templateFormat = to;

            FieldSimple = new Hashtable();
            FieldSimple.Add(Fields.responsesize, 0);
            FieldSimple.Add(Fields.statuscode, 1);
            FieldSimple.Add(Fields.cachestatus, 2);
            FieldSimple.Add(Fields.timetaken, 4);

            FieldComplex = new Hashtable();
            FieldComplex.Add(Fields.httpmethod, 0);
            FieldComplex.Add(Fields.uripath, 1);
            FieldComplex.Add(Fields.protocol, 2);
        }


        public string MinhaCDNBuilder()
        {
            return string.Format(
                "#Version: {0}\n" +
                "#Date: {1}\n" +
                "#Fields: {2}\n",
                "1.0",
                string.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now),
                string.Join(" ", templateFormat.Order.ToArray()));
        }

        //#Version: 1.0 
        //#Date: 15/12/2017 23:01:06 
        //#Fields: provider http-method status-code uri-path time-taken response-size cache-status 
        //"MINHA CDN" GET 200 /robots.txt 100 312 HIT 
        //"MINHA CDN" POST 200 /myImages 319 101 MISS 
        //"MINHA CDN" GET 404 /not-found 143 199 MISS 
        //"MINHA CDN" GET 200 /robots.txt 245 312 REFRESH_HIT

        public string Converter(string[] fileReceived)
        {
            StringBuilder build = new StringBuilder();

            foreach (var line in fileReceived)
            {
                foreach (string item in templateFormat.Order)
                {
                    build.Append(" ");
                    build.Append(FormatAdapter(line, item));
                    build.Append(" ");
                }
                build.Append("\n");
            }
            return build.ToString();
        }



        public string FormatAdapter(string originalLine, string position)
        {
            if (FieldSimple.Contains(position)) return SimpleAdapter(originalLine, position);
            else if (FieldComplex.Contains(position)) return ComplexAdapter(originalLine, position);
            else if (position.ToLower() == Fields.provider) return Provider;
            else return string.Format("Field {0} not found", position);
        }

        public string SimpleAdapter(string originalLine, string position)
        {
            return originalLine.Split(Separator)[Convert.ToInt32(FieldSimple[position].ToString())];
        }
        public string ComplexAdapter(string originalLine, string position)
        {
            return originalLine.Split(Separator)[3].Split(' ')[Convert.ToInt32(FieldComplex[position].ToString())].Replace("\"", "");
        }

    }
}
