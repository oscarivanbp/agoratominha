﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prueba.Services;
using Prueba.Formats;

namespace Prueba.models
{
    class AgoraFormatService : AgoraFormatOrder
    { 
        public List<string> Order { get; set; }

    public AgoraFormatService()
    {
        Order = new List<string>();
        Order.Add(Constants.Fields.provider);
        Order.Add(Constants.Fields.httpmethod);
        Order.Add(Constants.Fields.statuscode);
        Order.Add(Constants.Fields.uripath);
        Order.Add(Constants.Fields.timetaken);
        Order.Add(Constants.Fields.responsesize);
        Order.Add(Constants.Fields.cachestatus);
    }
    }
}
