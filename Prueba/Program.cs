﻿using System;
using System.Linq;


namespace Prueba
{
    class Program
    {

        public static void Main(string[] args)
        {
            do{
                Console.WriteLine("Ingresa porfavor una url valida a comsumir y una ruta y nombre para archivo de salida");

                string input = Console.ReadLine();

                string[] values = input.Trim().Split(' ');
                Solution s = new Solution();

                if (values.Count() == 2)
                {
                    Console.Write(s.StartConverter(values[0], values[1]));
                    Console.ReadLine();
                    break;
                }
                else
                    Console.Write("Please enter valid url and destination file\n Try again\n");
            } while (true) ;



        }
    }
}
