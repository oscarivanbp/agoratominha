﻿using System;
using System.Net;



using System;
using System.IO;
using System.Net;
using System.Configuration;

namespace Prueba.Services
{
    public class readRoute
    {

        protected string _sourceUrl { get; set; }
        protected string _targetPath { get; set; }
        protected string _temporaryPath { get; set; }
        protected string _temporaryFileName { get; set; }
        public Boolean read(string url, string fileName)
        {
            
            bool response = false;
            if (url == "" || fileName == "")
            {
                response =  false;

            } else if (url != null && fileName != null )
            {
                _sourceUrl = url;
                _targetPath = fileName;
                
                string remoteUri = url;
                string myStringWebResource = null;
                WebClient myWebClient = new WebClient();
                myStringWebResource = remoteUri;
                myWebClient.DownloadFile(myStringWebResource, fileName);
                Console.WriteLine(myStringWebResource);
                response = true;
            }
            return response;       
        }

        public string[] getFiles()
        {
            //int counter = 0;
            //string line;

            //System.IO.StreamReader file =
            //    new System.IO.StreamReader(fileName);
            //while ((line = file.ReadLine()) != null)
            //{
            //    System.Console.WriteLine(line);
            //    counter++;
            //}
            //file.Close();
            return File.ReadAllLines(_targetPath);
        }

        public string save(string converted)
        {
            if (!File.Exists(_targetPath)) { 
            File.Create(_targetPath); }
            File.Delete(_targetPath);
            try
            {
                string path = _targetPath;
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(converted);
                }
                return "ok";
            }
            catch
            {
                return "false";
            }
        }
     
    }
}

