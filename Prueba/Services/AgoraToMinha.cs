﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prueba.Formats;

namespace Prueba.Services
{
    class AgoraToMinha
    {
            agoraFinalFormat AgoraToMinhaConverter { get; set; }
            public AgoraToMinha(agoraFinalFormat finalFormat)
            {
            AgoraToMinhaConverter = finalFormat;
            }
            public string Execute(string[] lines)
            {
                return AgoraToMinhaConverter.MinhaCDNBuilder() + AgoraToMinhaConverter.Converter(lines);
            }
        
    
}
}
