﻿using System;
using System.IO;

namespace Prueba.Services
{
    class Validators
    {
        public static bool InputsValid(string url, string path)
        {
            path = "input.txt";
            if (!Uri.IsWellFormedUriString(url, UriKind.RelativeOrAbsolute)) { 
            return false; }
            if (!PathIsValid(path)){
            return false;
            }
            return true;
        }

        private static bool PathIsValid(string path)
        {
            bool valid = true;
            if (!path.ToLower().Contains(".txt"))
                valid = false;
            if (valid)
            {
                try
                {
                    if (!File.Exists(path))
                    {
                        File.Create(path);
                        
                    }
                    //if (!File.Exists(path))
                    //    valid = false;
                    File.Delete(path);
                }
                catch (Exception)
                {
                    valid = false;
                }
            
            }
            return valid;
        }

    }
}
